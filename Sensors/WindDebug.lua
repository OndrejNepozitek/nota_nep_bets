local sensorInfo = {
	name = "WindDebug",
	desc = "Shows wind direction",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
	if #units > 0 then
		local unitID = units[1]
		local x,y,z = Spring.GetUnitPosition(unitID)
		local wind = Sensors.map.Wind()

		if (Script.LuaUI('exampleWindDebug_update')) then
			Script.LuaUI.exampleWindDebug_update(
				unitID, -- key
				{	-- data
					unitId = unitID, 
					windFunc = Sensors.map.Wind
				}
			)
		end
		return {	-- data
					startPos = Vec3(x,y,z), 
					endPos = Vec3(x,y,z) + Vec3(10 * wind.dirX,0,10 * wind.dirZ)
				}
	end
end