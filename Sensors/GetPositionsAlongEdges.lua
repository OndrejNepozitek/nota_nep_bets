local sensorInfo = {
	name = "GetPositionsAlongEdges",
	desc = "Gets positions to explore. Sensor adapted from bentosming.",
	author = "bentosming",
	date = "2019-04-23",
	license = "MIT",
}

--TODO
local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unitCount)
	
	local unitPerSide = unitCount/4 
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local positions = {}
	local j = 1

	for x = 1, unitCount - 2 * unitPerSide + 1 do
		positions[j] = Vec3(maxX/(unitCount - 2 * unitPerSide + 1)*x, 100, maxZ)
		j = j+1
	end

	local sideOffset = 2000
	for z = 1, unitPerSide do
		positions[j] = Vec3(0, 100, sideOffset + (maxZ - sideOffset)/unitPerSide*z)
		j = j+1
		positions[j] = Vec3(maxX, 100, sideOffset + (maxZ - sideOffset)/unitPerSide*z)
		j = j+1
	end
		

	return positions
end

