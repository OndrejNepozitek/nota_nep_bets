local sensorInfo = {
	name = "GetUnitPosition",
	desc = "Returns position of a given unit.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition

return function(unit)
	local x,y,z = SpringGetUnitPosition(unit)
	return Vec3(x,y,z)
end