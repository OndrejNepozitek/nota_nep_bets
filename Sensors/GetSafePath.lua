local sensorInfo = {
	name = "GetSafePath",
	desc = "Based on a given safe map, computes safe path to a given goal.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local PositionToTile = Sensors.nota_nep_bets.PositionToTile
local TileToPosition = Sensors.nota_nep_bets.TileToPosition

return function(goal, safeMap)
	local iGoal, jGoal = unpack(PositionToTile(goal.x, goal.z, safeMap.granularity))

	local previousNode = {iGoal, jGoal}
	local gridPath = {previousNode}

	while previousNode ~= nil do
		local i = previousNode[1]
		local j = previousNode[2]

		previousNode = safeMap.values[i][j].previousNode
		gridPath[#gridPath+1] = previousNode
	end

	local actualPath = {}
	for i=0,#gridPath-1 do
		local pathTile = gridPath[#gridPath-i]
		actualPath[i+1] = TileToPosition(pathTile[1], pathTile[2], safeMap.granularity)
	end
	
	return actualPath
end