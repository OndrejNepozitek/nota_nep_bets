local sensorInfo = {
	name = "TrackEnemyUnits",
	desc = "Tracks all seen enemy units. It is enough to see an enemy once to know that it is/was there.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGetUnitDefID = Spring.GetUnitDefID
local trackedUnits = {}

return function()
	local enemyUnits = Sensors.core.EnemyUnits()

	for i=1, #enemyUnits do
		local enemyUnit = enemyUnits[i]
		local unitDefID = SpringGetUnitDefID(enemyUnit)
		local x, y, z = SpringGetUnitPosition(enemyUnit)

		if trackedUnits[enemyUnit] == nil then
			trackedUnits[enemyUnit] = {}
		end

		if x ~= nil then
			trackedUnits[enemyUnit].position = Vec3(x, y, z)
		end

		if unitDefID ~= nil then
			trackedUnits[enemyUnit].unitDefID = unitDefID
		end
	end

	return trackedUnits
end