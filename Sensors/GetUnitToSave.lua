local sensorInfo = {
	name = "GetUnitToSave",
	desc = "Finds a unit that needs saved and is not being saved by anyone.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local GetSafeMap = Sensors.nota_nep_bets.GetSafeMap
local SpringGetUnitPosition = Spring.GetUnitPosition
local PositionToTile = Sensors.nota_nep_bets.PositionToTile

local unitsBeingSaved = {}

function getUnitToSave(transporter, saveAreaCenter, saveAreaRadius, threatMap, tolerance, myUnits, groundUnits)
	local tx, ty, tz = SpringGetUnitPosition(transporter)
	local transporterPosition = Vec3(tx, ty, tz)
	local safeMap = GetSafeMap(transporterPosition, threatMap, tolerance)
	local unitToSave = nil
	local distance = 0

	for i=1, #groundUnits do
		local unit = groundUnits[i]
		local x, y, z = Spring.GetUnitPosition(unit)
		local position = Vec3(x, y, z)
		local i, j = unpack(PositionToTile(x, z, threatMap.granularity))

		local dd = transporterPosition:Distance(position)

		if (position:Distance(saveAreaCenter) > saveAreaRadius and unitsBeingSaved[unit] == nil and safeMap.values[i][j].distance ~= -1) then
			if (dd < distance or unitToSave == nil) then
				unitToSave = unit
				distance = dd
			end
		end
	end
	
	if (unitToSave ~= nil) then
		unitsBeingSaved[unitToSave] = true
	end
	
	return { 
		unit = unitToSave,
		safeMap = safeMap,
	}
end

local dangerTolerance = 0

return function(transporter, saveAreaCenter, saveAreaRadius, threatMap)
	local myUnits = Spring.GetTeamUnits(Spring.GetMyTeamID())
	local groundUnits = Sensors.core.FilterUnitsByCategory(myUnits, Categories.Common.groundUnits)
	local notAllSaved = false

	for i=1, #groundUnits do
		local unit = groundUnits[i]
		if (unitsBeingSaved[unit] == nil) then
			notAllSaved = true
		end
	end

	if (notAllSaved == false) then
		return {}
	end

	local unitInfo = getUnitToSave(transporter, saveAreaCenter, saveAreaRadius, threatMap, dangerTolerance, myUnits, groundUnits)

	if (unitInfo ~= nil) then
		dangerTolerance = 0
		return unitInfo
	else 
		if (dangerTolerance < 3) then
			dangerTolerance = dangerTolerance + 1
		end
	end

	return { }
end