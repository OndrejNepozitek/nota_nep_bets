local sensorInfo = {
	name = "GetHighestHill",
	desc = "Returns elevated positions.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

return function(unit)
	local x, y, z = Spring.GetUnitPosition(unit)
	local height = SpringGetGroundHeight(x, z)

	Spring.Echo(y)
	Spring.Echo(height)

	return height
end