local sensorInfo = {
	name = "SafeMapDebug",
	desc = "Visualizes threat map.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(threatMap)
	if (Script.LuaUI('threatMap_update')) then
		Script.LuaUI.threatMap_update(
			threatMap
		)
	end
end