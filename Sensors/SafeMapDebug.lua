local sensorInfo = {
	name = "SafeMapDebug",
	desc = "Visualizes safe map.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(safeMap)
	if (Script.LuaUI('safeMap_update')) then
		Script.LuaUI.safeMap_update(
			safeMap
		)
	end
end