local sensorInfo = {
	name = "GetElevatedPositions",
	desc = "Returns elevated positions.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

return function(targetElevation, minDistance)
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local step = 20
	local foundPositions = {}

	for x=0,maxX,step do
		for z=0,maxZ,step do
			local height = SpringGetGroundHeight(x, z)

			if (height == targetElevation) then
				local position = Vec3(x, height, z)
				local tooClose = false

				for i,otherPosition in ipairs(foundPositions) do
					if (position:Distance(otherPosition) < minDistance) then
						tooClose = true
						break
					end
				end

				if (tooClose == false) then
					foundPositions[#foundPositions+1] = position + Vec3(50, 0, 50)
				end
			end
		end	
	end

	return foundPositions
end