local sensorInfo = {
	name = "GetSafeMap",
	desc = "Computes where we can get without being attacked by enemy units.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 10 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitPosition = Spring.GetUnitPosition
local PositionToTile = Sensors.nota_nep_bets.PositionToTile
local TileToPosition = Sensors.nota_nep_bets.TileToPosition
local maxX = Game.mapSizeX
local maxZ = Game.mapSizeZ

function getNeighbours(i, j, threatMap)
	local possibleNeighbours = {{i, j + 1}, {i + 1, j}, {i, j - 1}, {i - 1, j}, {i + 1, j + 1}, {i + 1, j - 1}, {i - 1, j - 1}, {i - 1, j + 1}}
	local validNeigbours = {}

	for i,v in ipairs(possibleNeighbours) do
		if threatMap.values[v[1]] ~= nil and threatMap.values[v[1]][v[2]] ~= nil then
			validNeigbours[#validNeigbours+1] = v
		end
	end

	return validNeigbours
end

return function(start, threatMap, tolerance)
	local iStart, jStart = unpack(PositionToTile(start.x, start.z, threatMap.granularity))

	if (tolerance == nil) then
		tolerance = 0
	end

	local dfsMap = {
		granularity = threatMap.granularity,
		cols = threatMap.cols,
		rows = threatMap.rows,
		values = {}
	}

	local frontier = {{iStart, jStart}}
	local next = {}

    for i=0,dfsMap.cols do
		dfsMap.values[i] = {}
      	for j=0,dfsMap.rows do
        	dfsMap.values[i][j] = {
				distance = -1
			}
     	end
    end

	dfsMap.values[iStart][jStart].distance = 0
	local counter = 1
	while 0 < #frontier do
        next = {}
        for _, node in ipairs(frontier) do
			local i = node[1]
			local j = node[2]
			local neighbours = getNeighbours(i, j, threatMap)

			for _, neighbour in ipairs(neighbours) do
				local iNeighbour = neighbour[1]
				local jNeighbour = neighbour[2]

				if dfsMap.values[iNeighbour][jNeighbour].distance == -1 and threatMap.values[iNeighbour][jNeighbour] <= tolerance  then
					dfsMap.values[iNeighbour][jNeighbour].distance = counter
					dfsMap.values[iNeighbour][jNeighbour].previousNode = node
					next[#next+1] = neighbour
				end
			end
        end
		frontier = next
		counter = counter + 1
	end

	return dfsMap
end