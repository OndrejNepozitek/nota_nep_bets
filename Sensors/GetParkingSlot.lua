local sensorInfo = {
	name = "GetParkingSlot",
	desc = "Gets an empty parking slot.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local nextSlotNumber = -1

return function(areaCenter, areaRadius, slotsPerRow)
	local squareSize = areaRadius * math.sqrt(2)
	local squareCorner = areaCenter - Vec3(squareSize / 2, 0, squareSize / 2)
	local slotSize = math.floor(squareSize / slotsPerRow)

	nextSlotNumber = nextSlotNumber + 1

	local row = math.floor(nextSlotNumber / slotsPerRow)
	local col = nextSlotNumber - math.floor(nextSlotNumber/slotsPerRow) * slotsPerRow

	return squareCorner + Vec3(row * slotSize + slotSize / 2, 0, col * slotSize + slotSize / 2)
end