local sensorInfo = {
	name = "EnemyUnitsInSphere",
	desc = "Returns list of enemy units in a given sphere",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- @description returns list of enemy units
return function(position, radius)
	local enemyTeams = Sensors.core.EnemyTeamIDs()
	local enemyUnits = {}
	
	for i=1, #enemyTeams do
		local teamID = enemyTeams[i]
		local thisTeamUnits = Spring.GetUnitsInSphere(position.x, position.y, position.z, radius, teamID)
		
		for u=1, #thisTeamUnits do
			enemyUnits[#enemyUnits + 1] = thisTeamUnits[u]
		end
	end
	
	return enemyUnits
end