local sensorInfo = {
	name = "TileToPosition",
	desc = "Computes map position corresponding to a given tile.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

return function(i, j, granularity)
	local x = i * granularity + granularity / 2
	local z = j * granularity + granularity / 2
	local y = SpringGetGroundHeight(x, z)

	return Vec3(x, y, z)
end