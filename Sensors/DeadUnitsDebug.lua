local sensorInfo = {
	name = "WindDebug",
	desc = "Shows wind direction",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- @description return current wind statistics
return function()
    local units = Spring.GetAllFeatures()

    for i = 1, #units do
        local unitID = units[i]
		local x,y,z = Spring.GetFeaturePosition(unitID)

		if (Script.LuaUI('exampleDebug_update') and FeatureDefs[unitID].reclaimable == true) then
			Script.LuaUI.exampleDebug_update(
				unitID, -- key
				{	-- data
					unitId = unitID, 
                    startPos = Vec3(x, y, z),
                    endPos = Vec3(x + 50, y, z + 50)
				}
			)
		end 
    end


end