local sensorInfo = {
	name = "GetAvailableTransporter",
	desc = "Gets an available transporter and assigns a given branch.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local usedTransporters = {}

return function(transporters, branch)
	for i=1, #transporters do
		local transporter = transporters[i]

		if ((usedTransporters[transporter] == nil or usedTransporters[transporter] == branch) and Spring.ValidUnitID(transporter) == true) then
			usedTransporters[transporter] = branch
			return transporter
		end
	end

	return nil
end