local sensorInfo = {
	name = "GetAvailableTransporter",
	desc = "Gets an available transporter and assigns a given branch.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = 1 -- cachining results for multiple calls in one AI frame

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

local usedTransporters = {}

return function(corridorPoints)
    local lastAlliedUnits = 1

	for i=1, #corridorPoints do
		local corridorPoint = corridorPoints[i]

        local enemyUnits = Sensors.nota_nep_bets.EnemyUnitsInSphere(corridorPoint, 150)
        local alliedUnits = Sensors.nota_nep_bets.AlliedUnitsInSphere(corridorPoint, 150)

        if (#alliedUnits > 1) then
            lastAlliedUnits = i
        end

        if (#enemyUnits > 1) then
            return {
                alliedUnits = lastAlliedUnits,
                enemyUnits = i
            }
        end
	end

	return {
        alliedUnits = lastAlliedUnits,
        enemyUnits = #corridorPoints
    }
end