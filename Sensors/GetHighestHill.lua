local sensorInfo = {
	name = "GetHighestHill",
	desc = "Returns elevated positions.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight

return function(targetElevation, minDistance)
	local maxX = Game.mapSizeX
	local maxZ = Game.mapSizeZ
	local step = 1
	local maxHeight = 0
	local maxPosition

	for x=0,maxX,step do
		for z=0,maxZ,step do
			local height = SpringGetGroundHeight(x, z)

			if (height > maxHeight) then
				maxPosition = Vec3(x, height, z)
				maxHeight = height
			end
		end	
	end

	return maxPosition
end