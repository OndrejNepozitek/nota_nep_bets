local sensorInfo = {
	name = "PositionToTile",
	desc = "Computes tile coordinates from a given map position and granularity.",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

local EVAL_PERIOD_DEFAULT = -1 -- actual, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local maxX = Game.mapSizeX
local maxZ = Game.mapSizeZ

return function(x, z, granularity)
	if x < 0 then
		x = 0
	end

	if x > maxX then
		x = maxX
	end

	if z < 0 then
		z = 0
	end

	if z > maxZ then
		z = maxZ
	end

	return { math.floor(x/granularity), math.floor(z/granularity) }
end