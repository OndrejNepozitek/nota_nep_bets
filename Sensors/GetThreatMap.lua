local sensorInfo = {
	name = "GetThreatMap",
	desc = "Divides map into smaller tiles and computes .",
	author = "OndrejNepozitek",
	date = "2019-04-17",
	license = "MIT",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

local EVAL_PERIOD_DEFAULT = 10

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speedups
local SpringGetGroundHeight = Spring.GetGroundHeight
local SpringGetUnitPosition = Spring.GetUnitPosition
local PositionToTile = Sensors.nota_nep_bets.PositionToTile
local TileToPosition = Sensors.nota_nep_bets.TileToPosition
local maxX = Game.mapSizeX
local maxZ = Game.mapSizeZ

function gridHeight(i, j, granularity)
	local x = i * granularity
	local z = j * granularity
	return SpringGetGroundHeight(x, z)
end

function heightsTooDifferent(i, j, granularity)
	local heights = {
		gridHeight(i, j, granularity),
		gridHeight(i + 1, j, granularity),
		gridHeight(i, j + 1, granularity),
		gridHeight(i + 1, j + 1, granularity)
	}
	
	local max = math.max(unpack(heights))
	local min = math.min(unpack(heights))

	return max - min > 300
end

return function(trackedUnits, granularity, flyHeight)
	local threatMap = {
		granularity = granularity,
		cols = math.ceil(maxX/granularity) - 1,
		rows = math.ceil(maxZ/granularity) - 1,
		values = {}
	}

    for i=0,threatMap.cols do
		threatMap.values[i] = {}
      	for j=0,threatMap.rows do
			threatMap.values[i][j] = 0
			
			if (heightsTooDifferent(i, j, threatMap.granularity)) then
				threatMap.values[i][j] = 10
			end
     	end
    end

	for id, info in pairs(trackedUnits) do
		if info.unitDefID ~= nil then
			local position = info.position
			local x, y, z = position.x, position.y, position.z
			local i, j = unpack(PositionToTile(x, z, granularity))

			for key, weapon in pairs(UnitDefs[info.unitDefID].weapons) do
				weaponDefID = weapon.weaponDef
				weaponDef = WeaponDefs[weaponDefID]
				range = weaponDef.range
				
				if range > 0 then
					if (range > 1500) then
						range = 1500
					end

					i1, j1 = unpack(PositionToTile(x - range, z - range, granularity))
					i2, j2 = unpack(PositionToTile(x + range, z + range, granularity))
	
					for ii=i1, i2 do
						for jj=j1, j2 do
							gridPosition = TileToPosition(ii, jj, granularity)
							gridPosition = gridPosition + Vec3(0, flyHeight, 0)

							if position:Distance(gridPosition) < range then
								if (range < 1000 or gridPosition.y > y) then
									threatMap.values[ii][jj] = threatMap.values[ii][jj] + 1
								end
							end					
						end
					end
				end
			end
		elseif info.position ~= nil then
			local position = info.position
			local x, y, z = position.x, position.y, position.z
			local i, j = unpack(PositionToTile(x, z, granularity))
			local range = 1000

			i1, j1 = unpack(PositionToTile(x - range, z - range, granularity))
			i2, j2 = unpack(PositionToTile(x + range, z + range, granularity))

			for ii=i1, i2 do
				for jj=j1, j2 do
					gridPosition = TileToPosition(ii, jj, granularity)
					gridPosition = gridPosition + Vec3(0, flyHeight, 0)

					if position:Distance(gridPosition) < range then
						threatMap.values[ii][jj] = threatMap.values[ii][jj] + 1
					end					
				end
			end
		end
	end

	return threatMap
end