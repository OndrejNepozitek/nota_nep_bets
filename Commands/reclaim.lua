function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move unit along a given path.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit
local SpringGetUnitCommands = Spring.GetUnitCommands 

local function ClearState(self)
	self.started = false
end

function Run(self, units, parameter)
	local unit = parameter.unit
	local position = parameter.position
	local radius = parameter.radius

	-- pick the spring command implementing the move
	local cmdID = CMD.RECLAIM

	if (Spring.ValidUnitID(unit) == false) then
		return FAILURE
	end

	if (self.started ~= true) then
		SpringGiveOrderToUnit(unit, cmdID, { position.x, position.y, position.z, radius }, {})
		self.started = true
    else
        if (#SpringGetUnitCommands(unit, 1) == 0) then
            return SUCCESS
        end
    end

	return RUNNING
end

function Reset(self)
	ClearState(self)
end
