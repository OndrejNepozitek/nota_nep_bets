function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Move unit along a given path.",
		parameterDefs = {
			{ 
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "fight",
				variableType = "expression",
				componentType = "checkBox",
				defaultValue = "false",
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 0

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.nextPositionIndex = 1
	self.startedMoving = false
end

function Run(self, units, parameter)
	local unit = parameter.unit
	local path = parameter.path -- array of Vec3
	local fight = parameter.fight -- boolean

	if (#path == 0) then
		return FAILURE
	end

	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE
	if (fight) then cmdID = CMD.FIGHT end

	if (Spring.ValidUnitID(unit) == false) then
		return FAILURE
	end

	if (self.startedMoving ~= true) then
		SpringGiveOrderToUnit(unit, cmdID, path[1]:AsSpringVector(), {})
		self.startedMoving = true
		self.nextPositionIndex = 1
	end

	local pointX, pointY, pointZ = SpringGetUnitPosition(unit)
	local unitPosition = Vec3(pointX, pointY, pointZ)
	local distanceEpsilon = 400

	if (unitPosition:Distance(path[self.nextPositionIndex]) < distanceEpsilon) then
		if (self.nextPositionIndex == #path) then
			return SUCCESS
		end

		self.nextPositionIndex = self.nextPositionIndex + 1
		SpringGiveOrderToUnit(unit, cmdID, path[self.nextPositionIndex]:AsSpringVector(), {})

		return RUNNING
	end

	return RUNNING
end

function Reset(self)
	ClearState(self)
end
