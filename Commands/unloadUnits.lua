function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units at a given position.",
		parameterDefs = {
			{ 
				name = "units", -- which units to unload
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "transporter", -- which transporter to unload
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "position", -- where to unload units
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{ 
				name = "radius", -- radius of unload position
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
		}
	}
end

local function ClearState(self)
	self.commandGiven = false
end

function Run(self, allUnits, parameter)
	local units = parameter.units
	local transporter = parameter.transporter
	local position = parameter.position
	local radius = parameter.radius
	
	local allUnloaded = true

	if self.commandGiven ~= true then
		Spring.GiveOrderToUnit(transporter, CMD.UNLOAD_UNITS, {position.x, position.y, position.z, radius}, {});
		self.commandGiven = true
	end

	for i,unit in ipairs(units) do
		if Spring.GetUnitTransporter(unit) ~= nil then
			allUnloaded = false
		end
	end

	if allUnloaded == true then
		return SUCCESS
	else
		return RUNNING
	end
end

function Reset(self)
	ClearState(self)
end
